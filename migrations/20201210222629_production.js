exports.up = function (knex) {
    return Promise.all([
        knex.schema.createTable('production', function (table) {
            table.increments('id').primary();
            table.string('material_name');
            table.double('weight');
            table.date('date');
            table.time('time');
            table.boolean('isActive');
            table
                .timestamp('created_at')
                .notNullable()
                .defaultTo(knex.fn.now());
            table
                .timestamp('updated_at')
                .notNullable()
                .defaultTo(knex.fn.now());
        })
    ]);
};

exports.down = function (knex) {
    return Promise.all([knex.schema.dropTable('production')]);
};

