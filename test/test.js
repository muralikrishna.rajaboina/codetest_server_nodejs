const test = require('ava');
const request = require('supertest');
const app = require('../app');

test('signIn:invalid body values', async t => {
    t.plan(2);
    const res = await request(app)
        .post('/user/signIn')
        .send({ email: "murali@gmail.com", password: "dd" });
    t.is(res.status, 404);
    t.deepEqual(res.body, 'User not Existed');
});
test('signIn:invalid body properties', async t => {
    t.plan(2);
    const res = await request(app)
        .post('/user/signIn')
        .send({ username: "murali@gmail.com", password: "dd" });
    t.is(res.status, 400);
    t.deepEqual(res.body, "bad request");
});

test('signIn: Something wrong Internal', async t => {
    t.plan(1);
    const res = await request(app)
        .post('/user/signIn')
        .send({ email: "muraffi@gmail.com", password: "dd" });
    t.is(res.status, 500);

});

test('signIn: Success', async t => {
    t.plan(1);
    const res = await request(app)
        .post('/user/signIn')
        .send({ email: "murali@gmail.com", password: "abc" });
    t.is(res.status, 200);


});

test('search by material_name and Date: Get All', async t => {

    t.plan(1);
    const res = await request(app)
        .get('?pageSize=10&pageIndex=0&materialName=M2&date=16-12-2020')

    t.is(res.status, 200);
});

