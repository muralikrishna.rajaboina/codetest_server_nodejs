var express = require('express');
var router = express.Router();
const knex = require('knex');
const dbConfig = require('../knexfile').development;
const moment = require('moment');


/* add products to product database*/
router.post('/', async (req, res, next) => {
  let db;
  try {
    db = knex(dbConfig);
    console.log(req.body)
    const product = await db.returning('*')
      .insert({
        material_name: req.body.material_name,
        weight: req.body.weight,
        date: req.body.date,
        time: req.body.time,
        isActive: true
      })
      .into('production')

    await db.destroy();
    if (product) {
      res.status(200).json({ data: product[0], message: ' material added sucessfully' });
    }
    else {
      return res.status(500).json({ message: 'material not added' });
    }
  }
  catch (error) {
    await db.destroy();
    return res.status(500).json({ message: 'something wrong' });
  }
})
// get data with pagination and filter by material_name and date
router.get('/production', (req, res) => {
  const db = knex(dbConfig);
  const pageSize = parseInt(req.query.pageSize);
  const pageIndex = parseInt(req.query.pageIndex);

  const query = db
    .select(
      'id',
      'material_name',
      'weight',
      db.raw(`to_char(date,'DD-Mon-YYYY') as date`),
      db.raw(`to_char(time,'HH12:MI AM') as time`)
    )
    .from('production')
    .where({ 'isActive': true })
    .limit(pageSize)
    .offset(pageSize * pageIndex)
    .orderBy('date', 'desc')
    .orderBy('time', 'desc');

  const Count = db('production').count('id');

  if (req.query.materialName && req.query.materialName !== '') {
    query.where('material_name', 'ilike', '%' + req.query.materialName + '%');
    Count.where('material_name', 'ilike', '%' + req.query.materialName + '%');
  }

  if (req.query.date && req.query.date !== '') {
    query.where('date', req.query.date);
    Count.where('date', req.query.date);
  }

  const promData = Promise.all([query, Count])
    .then(results => {
      db.destroy();

      return res.json({
        data: results[0],
        count: parseInt(results[1][0].count)
      });
    })
    .catch(e => {
      db.destroy();
      return res.status(500).json(e);
    });
});

// bar chart data production of submaterials current_date

var d = new Date();
var currentDay = d.getDate();
var currentMonth = parseInt(d.getMonth()) + 1;
var currentYear = parseInt(d.getFullYear());
var currentDate = currentYear + '-' + currentMonth + '-' + currentDay;


router.get('/barchart', async (req, res) => {
  let db;
  try {
    db = knex(dbConfig);
    // const resolution = 'extract(month from "date")';
    const getDay = 'extract(year from "date")';
    const chartData = await db.select(db.raw(`count('id as count')`), 'material_name')
      .from('production')
      .where('date', currentDate)
      .groupBy('material_name')


    await db.destroy();
    if (chartData) {

      return res.status(200).json({
        lables: chartData.map(x => x.material_name),
        values: chartData.map(x => parseInt(x.count))
      })

    }
    else {
      return res.status(404).json({ message: 'not found' })
    }
  }

  catch (error) {
    await db.destroy();
    return res.status(500).json(error);
  };
});

// production of materials by current month
router.get('/piechart', async (req, res) => {
  let db;
  try {
    db = knex(dbConfig);

    const getMonth = 'extract(month from "date")';

    const chartData = await db.select(db.raw(`count('id as count')`), 'material_name')
      .from('production')
      .where(db.raw(`${getMonth}`), currentMonth)
      .groupBy('material_name')
    console.log(chartData)

    await db.destroy();
    if (chartData) {

      return res.status(200).json({
        lables: chartData.map(x => x.material_name),
        values: chartData.map(x => parseInt(x.count))
      })

    }
    else {
      return res.status(404).json({ message: 'not found' })
    }
  }

  catch (error) {
    await db.destroy();
    return res.status(500).json(error);
  };
});

// production of materials by current year
router.get('/linechart', async (req, res) => {
  let db;
  try {
    db = knex(dbConfig);

    const getYear = 'extract(year from "date")';
    const getMonth = 'extract(month from "date")';
    const chartData = await db.select(db.raw(`${getMonth} as month,count('id as count')`))
      .from('production')
      .where(db.raw(`${getYear}`), currentYear)
      .groupBy(db.raw(`${getMonth}`))
    console.log(chartData)

    await db.destroy();
    if (chartData) {

      return res.status(200).json({
        lables: chartData.map(x => moment(x.month, ['MM']).format('MMM')),
        values: chartData.map(x => parseInt(x.count))
      })

    }
    else {
      return res.status(404).json({ message: 'not found' })
    }
  }

  catch (error) {
    await db.destroy();
    return res.status(500).json(error);
  };
});
module.exports = router;
