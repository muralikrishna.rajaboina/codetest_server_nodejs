// Update with your config settings.

module.exports = {
  development: {
    client: 'pg',
    connection: {
      database: 'test',
      host: 'localhost',
      user: 'postgres',
      password: 'Design_20'
    }
  },

  staging: {
    client: 'pg',
    connection: {
      database: 'test',
      user: 'postgres',
      password: 'Design_20'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'migrations'
    }
  },

  production: {
    client: 'pg',
    connection: {
      database: 'test',
      user: 'postgres',
      password: 'Design_20'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'migrations'
    }
  }
};
